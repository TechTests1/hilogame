﻿// See https://aka.ms/new-console-template for more information
using Gaming1Casino;
using Gaming1Casino.Commands.CommandsFactory;
using Gaming1Casino.Games;
using Gaming1Casino.Interactors;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureServices(services =>
    {
        services.AddScoped<IInteractor, ConsoleInteractor>();
        services.AddScoped<IMysteryNumberStrategy, RandomMysteryNumberStrategy>();
        services.AddScoped<ICommandFactory, CommandFactory>();
        services.AddSingleton<Casino>();
    })
    .Build();

var casino = host.Services.GetService<Casino>();
casino.Start();

await host.RunAsync();