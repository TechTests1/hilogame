﻿using Gaming1Casino.Commands.CommandsFactory;
using Gaming1Casino.Games;
using Gaming1Casino.Interactors;

namespace Gaming1Casino
{
    public class Casino
    {
        private Dictionary<string, IGame> _games = new Dictionary<string, IGame> ();
        private ICommandFactory _commandsFactory;
        private IInteractor _interactor;

        public Casino(ICommandFactory commandsFactory, IInteractor interactor)
        {
            _commandsFactory = commandsFactory;
            _interactor = interactor;
        }

        public void Start()
        {
            bool allGamesFinished = false;
            do
            {
                _interactor.Write("Introduce command");
                var stringCommand = _interactor.ReadString();

                var command = _commandsFactory.CreateCommand(stringCommand, _games.Values.ToList());
                if (command is null)
                {
                    _interactor.Write("Command not valid"); 
                    continue;
                }
                var commandResult = command.Execute();

                _games[commandResult.Game.Id] = commandResult.Game;
                _interactor.Write(commandResult.Message);

                if (commandResult.Game.Finished)
                {
                    _games.Remove(commandResult.Game.Id);
                }

                allGamesFinished = _games.All(game => game.Value.Finished);
            } while (!allGamesFinished);

            _interactor.Write("All games have finished");
        }
    }
}
