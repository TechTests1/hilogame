﻿using Gaming1Casino.Games;

namespace Gaming1Casino.Interactors
{
    public interface IInteractor
    {
        int ReadNumber();
        string ReadString();
        void Write(string message);
    }
}
