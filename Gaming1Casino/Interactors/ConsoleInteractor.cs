﻿namespace Gaming1Casino.Interactors
{
    public class ConsoleInteractor : IInteractor
    {
        public int ReadNumber()
        {
            var stringValue = Console.ReadLine();
            int numericValue = 0;
            var isNumeric = int.TryParse(stringValue, out numericValue);
            while (!isNumeric)
            {
                Console.WriteLine("Introduce a valid numeric value");
                stringValue = Console.ReadLine();
                isNumeric = int.TryParse(stringValue, out numericValue);
            }

            return numericValue;
        }

        public string ReadString()
        {
            var gameId = Console.ReadLine();
            return gameId;
        }

        public void Write(string message)
        {
            Console.WriteLine(message);
        }
    }
}
