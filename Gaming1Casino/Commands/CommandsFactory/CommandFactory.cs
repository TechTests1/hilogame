﻿using Gaming1Casino.Games;
using Gaming1Casino.Interactors;

namespace Gaming1Casino.Commands.CommandsFactory
{
    public class CommandFactory : ICommandFactory
    {
        private IInteractor _interactor;
        private IMysteryNumberStrategy _mysteryNumberStrategy;

        public CommandFactory(IInteractor interactor, IMysteryNumberStrategy mysteryNumberStrategy)
        {
            _interactor = interactor;
            _mysteryNumberStrategy = mysteryNumberStrategy;
        }

        public ICommand CreateCommand(string stringCommand, List<IGame> games)
        {
            switch (stringCommand)
            {
                case "Start":
                    _interactor.Write("Introduce game identifier");
                    var gameId = _interactor.ReadString();

                    _interactor.Write("Introduce minimum value");
                    var min = _interactor.ReadNumber();

                    _interactor.Write("Introduce maximum value");
                    var max = _interactor.ReadNumber();

                    var players = ReadPlayers();
                    return new StartGameCommand(gameId, min, max, players, _mysteryNumberStrategy);

                case "Play":
                    var game = GetCurrentGame(games);

                    if(game is null)
                    {
                        _interactor.Write("No game has started yet");
                        return null;
                    }

                    string player = null;
                    if(game.IsMultiPlayer())
                    {
                        _interactor.Write("Introduce player identifier");
                        player = _interactor.ReadString();
                    }
                    
                    _interactor.Write("Introduce number");
                    var number = _interactor.ReadNumber();                    

                    return new PlayCommand(number, game, player);

                case "Cancel":
                    game = GetCurrentGame(games);
                    if (game is null)
                    {
                        _interactor.Write("No game has started yet");
                        return null;
                    }

                    return new CancelGameCommand(game);

                default:
                    return null;
            }
        }

        private List<string> ReadPlayers()
        {
            _interactor.Write("How many players?");
            var playersCount = _interactor.ReadNumber();

            var players = new List<string>();
            for(int i =0; i<playersCount; i++)
            {
                _interactor.Write("Introduce player identifier");
                var player = _interactor.ReadString();
                
                players.Add(player);
            }

            return players;
        }

        private IGame GetCurrentGame(List<IGame> games)
        {
            var game = games.FirstOrDefault();
            if (games.Count > 1)
            {
                _interactor.Write("Introduce game identifier");
                var gameId = _interactor.ReadString();
                game = games.FirstOrDefault(x => x.Id == gameId);
            }

            return game;
        }
    }
}
