﻿using Gaming1Casino.Games;

namespace Gaming1Casino.Commands.CommandsFactory
{
    public interface ICommandFactory
    {
        ICommand CreateCommand(string stringCommand, List<IGame> games);
    }
}
