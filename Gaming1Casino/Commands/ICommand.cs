﻿using Gaming1Casino.Games;

namespace Gaming1Casino.Commands
{
    public interface ICommand
    {
        CommandResult Execute();
    }
}
