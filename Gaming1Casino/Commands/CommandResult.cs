﻿using Gaming1Casino.Games;

namespace Gaming1Casino.Commands
{
    public class CommandResult
    {
        public IGame Game { get; set; }
        public string Message { get; set; }
    }
}
