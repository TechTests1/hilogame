﻿using Gaming1Casino.Games;

namespace Gaming1Casino.Commands
{
    public class CancelGameCommand : ICommand
    {
        public IGame Game { get; }

        public CancelGameCommand(IGame game)
        {
            Game = game;
        }

        public CommandResult Execute()
        {
            Game.Cancel();

            return new CommandResult
            {
                Game = Game,
                Message = $"Game '{Game.Id}' has been cancelled"
            };
        }
    }
}
