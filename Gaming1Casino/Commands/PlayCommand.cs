﻿using Gaming1Casino.Games;
using Gaming1Casino.Interactors;

namespace Gaming1Casino.Commands
{
    public class PlayCommand : ICommand
    {
        public int Number { get; }
        public IGame Game { get; }
        public string Player { get; }

        public PlayCommand(int number, IGame game, string player)
        {
            Number = number;
            Game = game;
            Player = player;
        }

        public CommandResult Execute()
        {
            var result = Game.Play(Number, Player);

            return new CommandResult
            {
                Game = Game,
                Message = result
            };
        }
    }
}
