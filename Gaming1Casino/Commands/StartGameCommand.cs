﻿using Gaming1Casino.Games;

namespace Gaming1Casino.Commands
{
    public class StartGameCommand : ICommand
    {
        private IMysteryNumberStrategy _mysteryNumberStrategy;

        public string GameId { get; }
        public int Min { get; }
        public int Max{get;}
        public List<string> Players { get; }

        public StartGameCommand(
            string gameId,
            int min,
            int max,
            List<string> players,
            IMysteryNumberStrategy mysteryNumberStrategy)
        {
            GameId = gameId;
            Min = min;
            Max = max;
            Players = players;
            _mysteryNumberStrategy = mysteryNumberStrategy;
        }

        public CommandResult Execute()
        {
            var game = new HiLoGame(GameId, Min, Max, Players, _mysteryNumberStrategy);
            game.Start();

            return new CommandResult
            {
                Game = game,
                Message = $"Game {game.Id} started"
            };
        }        
    }
}
