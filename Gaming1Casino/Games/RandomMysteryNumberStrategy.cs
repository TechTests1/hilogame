﻿namespace Gaming1Casino.Games
{
    public class RandomMysteryNumberStrategy : IMysteryNumberStrategy
    {
        public int GenerateMysteryNumber(int min, int max)
        {
            return new Random().Next(min, max);
        }
    }
}
