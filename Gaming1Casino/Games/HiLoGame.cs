﻿namespace Gaming1Casino.Games
{

    public class HiLoGame : IGame
    {
        private IMysteryNumberStrategy _mysteryNumberStrategy;
        private int _mysteryNumber;
        private int _minNumber;
        private int _maxNumber;

        private string _id { get; }
        public string Id => _id;

        private bool _finished = false;
        public bool Finished => _finished;

        private bool _playing = false;
        private List<Player> _players;

        public HiLoGame(
            string id, 
            int minNumber, 
            int maxNumber, 
            List<string> players, 
            IMysteryNumberStrategy mysteryNumberStrategy)
        {
            _minNumber = minNumber;
            _maxNumber = maxNumber;
            _id = id;
            _players = players.Select(player => new Player { Id = player }).ToList();
            _mysteryNumberStrategy = mysteryNumberStrategy;
        }

        public void Start()
        {
            _mysteryNumber = _mysteryNumberStrategy.GenerateMysteryNumber(_minNumber, _maxNumber);
            _playing = true;
        }

        public string Play(int number, string playerId)
        {
            if (!_playing)
            {
                return "Game not started";
            }

            var player =
                playerId is null
                    ? _players.FirstOrDefault()
                    : _players.FirstOrDefault(player => player.Id == playerId);

            if (player is null)
            {
                return "Player does not exists";
            }

            var result = GuessMysteryNumber(number);
            player.IncrementAttempts();

            return result;
        }

        public void Cancel()
        {
            _finished = true;
        }

        public bool IsMultiPlayer()
        {
            return _players.Count > 1;
        }

        private string GuessMysteryNumber(int number)
        {
            if (_mysteryNumber < number)
            {
                return "LO";
            }
            else if (_mysteryNumber > number)
            {
                return "HI";
            }
            else
            {
                _finished = true;
                return "WINNER";
            }
        }
    }
}
