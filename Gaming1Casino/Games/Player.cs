﻿namespace Gaming1Casino.Games
{
    public class Player
    {
        public string Id { get; set; }
        public int Attempts { get; set; }

        public void IncrementAttempts()
        {
            Attempts += 1;
        }
    }
}
