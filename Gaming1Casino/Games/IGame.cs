﻿namespace Gaming1Casino.Games
{
    public interface IGame
    {
        public string Id { get; }
        public bool Finished { get; }

        void Start();
        string Play(int number, string playerId);
        void Cancel();
        bool IsMultiPlayer();
    }
}
