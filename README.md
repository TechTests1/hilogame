# HiLoGame



## Getting started

The principle of the game is as follows:
1. The system chooses a number between [Min; Max] (the mystery number)
2. The player proposes a number between [Min; Max]
3. If the player's proposal is not the mystery number, the system tells the player whether:
   - HI: the mystery number is > the player's guess
   - LO: the mystery number is < the player's guess and the player plays again.
4. The goal of the game is to discover the mystery number in a minimum of iterations.

Finally, you are asked to add a multiplayer concept to the game. Think of this request as an
opportunity to show us that you can design a solution.