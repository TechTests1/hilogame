﻿using Gaming1Casino.Games;
using Moq;

namespace Gaming1Casino.Tests.Games
{
    public class HiLoGameTests
    {
        private HiLoGame _sut;
        private Mock<IMysteryNumberStrategy> _mysteryNumberStrategyMock = new Mock<IMysteryNumberStrategy>();
        private const string GameId = "G1";
        private const int Min = 1;
        private const int Max = 5;

        [Fact]
        public void WhenGameStarts_Then_ShouldGenerateMysteryNumber()
        {
            _sut = new HiLoGame(GameId, Min, Max, new List<string>(), _mysteryNumberStrategyMock.Object);

            _sut.Start();

            _mysteryNumberStrategyMock.Verify(x =>
            x.GenerateMysteryNumber(
                    It.Is<int>(x => x == Min),
                    It.Is<int>(x => x == Max)),
                Times.Once);
        }

        [Fact]
        public void Given_GameHasNotStarted_When_Playing_Then_ReturnsNotStartedError()
        {
            _sut = new HiLoGame(GameId, Min, Max, new List<string>(), _mysteryNumberStrategyMock.Object);

            var result = _sut.Play(1, null);
            Assert.Equal("Game not started", result);
        }

        [Fact]
        public void Given_GameHasStarted_When_PlayingWithNonExistingPlayer_Then_ReturnsPlayerError()
        {
            var players = new List<string>()
            {
                "P1"
            };
            _sut = new HiLoGame(GameId, Min, Max, players, _mysteryNumberStrategyMock.Object);
            _sut.Start();

            var result = _sut.Play(1, "P2");
            Assert.Equal("Player does not exists", result);
        }

        [Theory]
        [InlineData(2, 3, "HI", false)]
        [InlineData(4, 3, "LO", false)]
        [InlineData(3, 3, "WINNER", true)]
        public void Given_GameHasStarted_When_Playing_Then_ReturnsGuessResult_ExpectedFinished(
            int providedNumber,
            int mysteryNumber,
            string expectedResult,
            bool expectedFinished)
        {
            var playerId = "P1";
            var players = new List<string>()
            {
                playerId
            };

            _mysteryNumberStrategyMock
                .Setup(x => x.GenerateMysteryNumber(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(mysteryNumber);

            _sut = new HiLoGame(GameId, Min, Max, players, _mysteryNumberStrategyMock.Object);
            _sut.Start();

            var result = _sut.Play(providedNumber, playerId);
            Assert.Equal(expectedResult, result);
            Assert.Equal(_sut.Finished, expectedFinished);
        }

        [Fact]
        public void Given_GameHasStarted_WhenCancelled_ThenItShouldFinish()
        {
            _sut = new HiLoGame(GameId, Min, Max, new List<string>(), _mysteryNumberStrategyMock.Object);

            _sut.Start();
            _sut.Cancel();

            Assert.True(_sut.Finished);
        }

        [Fact]
        public void Given_GameHasStartedWithMultiplePlayers_Then_GameIsMultiplayer()
        {
            var players = new List<string>()
            {
                "P1",
                "P2"
            };
            _sut = new HiLoGame(GameId, Min, Max, players, _mysteryNumberStrategyMock.Object);
            _sut.Start();

            Assert.True(_sut.IsMultiPlayer());
        }

        [Fact]
        public void Given_GameHasStartedWithOnePlayere_Then_GameIsNotMultiplayer()
        {
            var players = new List<string>()
            {
                "P1"
            };
            _sut = new HiLoGame(GameId, Min, Max, players, _mysteryNumberStrategyMock.Object);
            _sut.Start();

            Assert.False(_sut.IsMultiPlayer());
        }
    }
}
